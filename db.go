package main

import (
	client "github.com/influxdata/influxdb1-client/v2"
)

// Maximum number of Points per batch, the InfluxDB API documentation suggests
// this value should be 5000-10000
const MaxBatch = 6000

func addPoints(cln client.Client, dbname string, ds DataSource, f Filter) (int, error) {
	count, bcount := int(0), int(0)
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  dbname,
		Precision: ds.Precision(),
	})
	if err != nil {
		return count, err
	}

	for ds.Next() {
		t, fields := ds.Measurement()
		pt, _ := client.NewPoint(ds.Name(),
			ds.Tags(),
			f(fields), t)
		bp.AddPoint(pt)
		count++
		bcount++
		if bcount == MaxBatch {
			err = cln.Write(bp)
			if err != nil {
				return 0, err
			}
			bp, _ = client.NewBatchPoints(client.BatchPointsConfig{
				Database:  dbname,
				Precision: ds.Precision(),
			})
			bcount = 0
		}
	}

	if bcount > 0 {
		err = cln.Write(bp)
		if err != nil {
			return 0, err
		}
	}
	return count, ds.Err()
}
