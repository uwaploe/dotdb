// Dotdb loads the contents of one or more DOT data files into
// an InfluxDB time series database.
package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"runtime"
	"strings"

	client "github.com/influxdata/influxdb1-client/v2"
	"golang.org/x/crypto/ssh/terminal"
)

var Version = "dev"
var BuildDate = "unknown"

var usage = `Usage: dotdb [options] dbname type file [file...]

Load the contents of one or more DOT data files into an InfluxDB.
`

var dataTypes map[string][]string = map[string][]string{
	"gps":    nil,
	"pr":     {"pressure"},
	"angles": {"heading", "pitch", "roll"},
	"env":    {"air temp", "barometer", "internal temp"},
	"eng":    {"battery", "vsolar", "vaux", "vprimary"},
	"gyro":   {"gyro.x", "gyro.y", "gyro.z"},
	"accel":  {"accel.x", "accel.y", "accel.z"},
}

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	dotId         string
	dbUrl         string = "http://localhost:8086"
	dbName        string
	dbUser        string
	dotDeployment string
)

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func getPassword(prompt string) ([]byte, error) {
	fd := int(os.Stdin.Fd())
	state, err := terminal.GetState(fd)
	if err != nil {
		return nil, err
	}

	ch := make(chan os.Signal)
	signal.Notify(ch, os.Interrupt, os.Kill)
	defer signal.Stop(ch)

	go func() {
		_, ok := <-ch
		if ok {
			terminal.Restore(fd, state)
			os.Exit(1)
		}
	}()

	fmt.Print(prompt)
	pass, err := terminal.ReadPassword(fd)
	fmt.Println("")

	return pass, err
}

func openClient() (client.Client, error) {
	cfg := client.HTTPConfig{
		Addr: dbUrl,
	}

	if dbUser != "" {
		creds := strings.Split(dbUser, ":")
		cfg.Username = creds[0]
		switch len(creds) {
		case 1:
			pass, err := getPassword("Database password: ")
			if err != nil {
				log.Fatal(err)
			}
			cfg.Password = string(pass)
		case 2:
			cfg.Password = creds[1]
		}
	}

	return client.NewHTTPClient(cfg)
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, usage)
		flag.PrintDefaults()
	}
	flag.StringVar(&dotId, "id", lookupEnvOrString("DOT_ID", dotId), "DOT Buoy ID")
	flag.StringVar(&dbUrl, "url", lookupEnvOrString("DOT_DB_URL", dbUrl), "InfluxDB url")
	flag.StringVar(&dbUser, "user", lookupEnvOrString("DOT_DB_USER", dbUser),
		"Database username:password")
	flag.StringVar(&dotDeployment, "deploy",
		lookupEnvOrString("DOT_DEPLOYMENT", dotDeployment), "DOT deployment name")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func storeFile(path string, cln client.Client, dbname, measurement string) (int, error) {
	fin, err := os.Open(path)
	if err != nil {
		return 0, err
	}
	defer fin.Close()

	var filter Filter
	if fields := dataTypes[measurement]; fields != nil {
		filter = IncludeFilter(fields...)
	} else {
		filter = NullFilter()
	}

	tags := map[string]string{
		"id":         dotId,
		"deployment": dotDeployment,
	}

	src := NewCsvSource(fin, measurement, tags)
	return addPoints(cln, dbname, src, filter)
}

func main() {
	args := parseCmdLine()
	if len(args) < 3 {
		flag.Usage()
		os.Exit(1)
	}

	dbname := args[0]
	measurement := args[1]

	_, ok := dataTypes[measurement]
	if !ok {
		log.Fatalf("Invalid data type: %q", measurement)
	}

	cln, err := openClient()
	if err != nil {
		log.Fatalf("Error creating InfluxDB Client: %v", err)
	}
	defer cln.Close()

	for _, arg := range args[2:] {
		n, err := storeFile(arg, cln, dbname, measurement)
		if err != nil {
			log.Printf("Error reading %s: %v", arg, err)
		}
		log.Printf("Stored %d points to %s", n, measurement)
	}
}
