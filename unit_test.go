package main

import (
	"strings"
	"testing"
	"time"
)

var CSV_DATA = `time,air temp (deg C),barometer (mbar),battery (V),Vsolar (V),Vaux (V),Vprimary (V),internal temp (deg C)
2019-09-18 03:01:11,-11.3,1028.23,15.68,11.35,15.66,15.66,-5.2
2019-09-18T03:16:11Z,-10.9,1028.25,15.64,11.36,15.65,15.64,-4.0
2019-09-18 03:31:05,-10.7,1028.12,15.66,11.36,15.65,15.66,-3.7
`

var CSV_DATA_NH = `2019-09-18 03:01:11,-11.3,1028.23,15.68,11.35,15.66,15.66,-5.2
2019-09-18T03:16:11Z,-10.9,1028.25,15.64,11.36,15.65,15.64,-4.0
2019-09-18 03:31:05,-10.7,1028.12,15.66,11.36,15.65,15.66,-3.7
`

func TestCsvRead(t *testing.T) {
	recs := make([]map[string]interface{}, 0)
	ts := make([]time.Time, 0)

	src := NewCsvSource(strings.NewReader(CSV_DATA), "", map[string]string{})
	for src.Next() {
		t, rec := src.Measurement()
		ts = append(ts, t)
		recs = append(recs, rec)
	}

	if len(recs) != 3 {
		t.Fatalf("Wrong record count: %d", len(recs))
	}

	v, ok := recs[1]["internal_temp"]
	if !ok {
		t.Fatalf("Missing value from record: %#v", recs[1])
	}
	if x, ok := v.(float64); !ok {
		t.Fatalf("Bad data value type: %T", v)
	} else if x != -4.0 {
		t.Errorf("Bad value; expected -4.0, got %f", x)
	}
}

func TestCsvNoHeader(t *testing.T) {
	recs := make([]map[string]interface{}, 0)
	ts := make([]time.Time, 0)

	src := NewCsvSource(strings.NewReader(CSV_DATA_NH), "", map[string]string{})
	for src.Next() {
		t, rec := src.Measurement()
		ts = append(ts, t)
		recs = append(recs, rec)
	}

	if len(recs) != 2 {
		t.Fatalf("Wrong record count: %d", len(recs))
	}

	v, ok := recs[0]["field_7"]
	if !ok {
		t.Fatalf("Missing value from record: %#v", recs[1])
	}
	if x, ok := v.(float64); !ok {
		t.Fatalf("Bad data value type: %T", v)
	} else if x != -4.0 {
		t.Errorf("Bad value; expected -4.0, got %f", x)
	}
}

func TestFilterInclude(t *testing.T) {
	f := IncludeFilter("barometer", "air temp")
	src := NewCsvSource(strings.NewReader(CSV_DATA), "", map[string]string{})
	for src.Next() {
		_, rec := src.Measurement()
		rec2 := f(rec)
		if len(rec2) != 2 {
			t.Errorf("Bad record length; expected 2, got %d", len(rec2))
		}
		if rec2["air_temp"] != rec["air_temp"] {
			t.Errorf("Wrong field value; expected %v, got %v",
				rec["air_temp"], rec2["air_temp"])
		}
	}
}

func TestFilterExclude(t *testing.T) {
	f := ExcludeFilter("barometer", "air temp")
	src := NewCsvSource(strings.NewReader(CSV_DATA), "", map[string]string{})
	for src.Next() {
		_, rec := src.Measurement()
		rec2 := f(rec)
		if (len(rec) - len(rec2)) != 2 {
			t.Errorf("Bad record length; expected 2, got %d", len(rec)-len(rec2))
		}
		if _, ok := rec2["air_temp"]; ok {
			t.Error("Unexpected field")
		}
	}
}

func sameList(a []string, b []string) bool {
	return strings.Join(a, "") == strings.Join(b, "")
}

func TestColumnNames(t *testing.T) {
	table := []struct {
		in, out []string
	}{
		{
			in:  []string{"heading (deg)", "pitch (deg)", "roll (deg)"},
			out: []string{"heading", "pitch", "roll"},
		},
		{
			in:  []string{"heading(deg)", "pitch(deg)", "roll(deg)"},
			out: []string{"heading", "pitch", "roll"},
		},
		{
			in:  []string{"heading", "pitch", "roll"},
			out: []string{"heading", "pitch", "roll"},
		},
	}

	for _, e := range table {
		cols := colsFromHeader(e.in)
		if !sameList(cols, e.out) {
			t.Errorf("Wrong output; expected %v, got %v", e.out, cols)
		}
	}
}
