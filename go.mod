module bitbucket.org/uwaploe/dotdb

require (
	github.com/influxdata/influxdb1-client v0.0.0-20190809212627-fc22c7df067e
	golang.org/x/crypto v0.0.0-20191119213627-4f8c1d86b1ba
)

go 1.13
