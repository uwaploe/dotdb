package main

import (
	"encoding/csv"
	"fmt"
	"io"
	"strconv"
	"strings"
	"time"
	"unicode"
	"unicode/utf8"
)

const TIMEFMT = "2006-01-02 15:04:05.999"
const ALT_TIMEFMT = "2006-01-02T15:04:05Z"

func rmSpaces(in string) string {
	return strings.ToLower(strings.Map(func(r rune) rune {
		if r == ' ' {
			return '_'
		}
		return r
	}, in))
}

// Filter is used to filter DataSource measurements
type Filter func(map[string]interface{}) map[string]interface{}

func NullFilter() Filter {
	return func(m map[string]interface{}) map[string]interface{} {
		return m
	}
}

func IncludeFilter(names ...string) Filter {
	fields := make(map[string]bool)
	for _, name := range names {
		fields[rmSpaces(name)] = true
	}
	return func(m map[string]interface{}) map[string]interface{} {
		m2 := make(map[string]interface{})
		for k, v := range m {
			if fields[k] {
				m2[k] = v
			}
		}
		return m2
	}
}

func ExcludeFilter(names ...string) Filter {
	fields := make(map[string]bool)
	for _, name := range names {
		fields[rmSpaces(name)] = true
	}
	return func(m map[string]interface{}) map[string]interface{} {
		m2 := make(map[string]interface{})
		for k, v := range m {
			if !fields[k] {
				m2[k] = v
			}
		}
		return m2
	}
}

// DataSource provides data records for an InfluxDB database
type DataSource interface {
	// Name returns the measurement name
	Name() string
	// Tags returns a map of tags and values
	Tags() map[string]string
	// Next advances to the next data record, returning
	// false at the end of the stream
	Next() bool
	// Err returns the most recent error
	Err() error
	// Measurement returns the current timestamp and data record
	Measurement() (time.Time, map[string]interface{})
	// Precision returns the timestamp precision
	Precision() string
}

type CsvSource struct {
	rdr  *csv.Reader
	t    time.Time
	err  error
	cols []string
	rec  map[string]interface{}
	tags map[string]string
	name string
}

func colsFromHeader(hdr []string) []string {
	var idx int
	cols := make([]string, len(hdr))
	for i, text := range hdr {
		idx = strings.Index(text, "(")
		if idx == -1 {
			idx = len(text)
		}
		cols[i] = rmSpaces(strings.TrimRight(text[0:idx], " \t"))
	}
	return cols
}

// Return a new CsvSource which reads from an underlying io.Reader
func NewCsvSource(rdr io.Reader, name string, tags map[string]string) *CsvSource {
	s := &CsvSource{
		tags: tags,
		rdr:  csv.NewReader(rdr),
		name: name,
	}
	var hdr []string
	hdr, s.err = s.rdr.Read()
	if s.err != nil {
		return s
	}

	// If header value starts with a digit, assume no header is present
	// and generate one.
	r, _ := utf8.DecodeRuneInString(hdr[0])
	if unicode.IsDigit(r) {
		s.cols = make([]string, len(hdr))
		for i := 0; i < len(hdr); i++ {
			s.cols[i] = fmt.Sprintf("field_%d", i)
		}
	} else {
		s.cols = colsFromHeader(hdr)
	}

	return s
}

func (s *CsvSource) Name() string {
	return s.name
}

func (s *CsvSource) Tags() map[string]string {
	return s.tags
}

func (s *CsvSource) Next() bool {
	var rec []string
	rec, s.err = s.rdr.Read()
	if s.err != nil {
		return false
	}

	s.rec = make(map[string]interface{})
	s.t, s.err = time.Parse(TIMEFMT, rec[0])
	if s.err != nil {
		s.t, s.err = time.Parse(ALT_TIMEFMT, rec[0])
		if s.err != nil {
			s.err = fmt.Errorf("Cannot parse timestamp %q: %w", rec[0], s.err)
			return false
		}
	}

	for i, x := range rec[1:] {
		if val, err := strconv.ParseInt(x, 10, 64); err == nil {
			s.rec[s.cols[i+1]] = val
		} else if val, err := strconv.ParseFloat(x, 64); err == nil {
			s.rec[s.cols[i+1]] = val
		} else {
			s.rec[s.cols[i+1]] = x
		}
	}
	return s.err == nil
}

func (s *CsvSource) Err() error {
	if s.err != io.EOF {
		return s.err
	}
	return nil
}

func (s *CsvSource) Measurement() (time.Time, map[string]interface{}) {
	return s.t, s.rec
}

func (s *CsvSource) Precision() string {
	return "ms"
}
